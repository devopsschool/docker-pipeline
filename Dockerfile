FROM ubuntu:latest

# Install Python and related tools
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get -y upgrade
RUN apt-get install -y python3
RUN apt-get install -y python3-pip
RUN pip3 install --upgrade pip

# Now use Pip to install Flask
RUN pip3 install flask
